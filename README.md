# FieldKit Firmware

## Introduction

Firmware for the FieldKit line of sensors

## Dependencies

- **Ubuntu**
- **Build Tools**: make, cmake, libboost-dev, git, and others are required for compiling the code.
- **Python 3**: Used for various utility scripts.
- **Python Packages**: wheel, sphinx, pyelftools, pyblake2, lief.
- **Arm-none-eabi Compiler Libraries**: lib32stdc++6, lib32z1.

## Usage

To use this project:

1. Clone the repository.
2. Install the required dependencies.
3. Use the provided Makefile for local development.
4. For CI/CD, create a pull request to the GitLab repository to trigger the pipeline.

## Contributing

Contributions to the project are welcome. Please follow the standard Git workflow - fork the repository, make changes, and submit a merge request.

## Contact

#### Jacob Lewallen | jacob@conservify.org

## License

See **LICENSE** document in main folder

