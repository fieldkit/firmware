module github.com/fieldkit/firmware/tools

go 1.17

require (
	github.com/fieldkit/app-protocol v0.0.0-20211012215823-de5c2d7c93cc
	github.com/fieldkit/data-protocol v0.0.0-20211007160527-621bd5b573e3
	github.com/golang/protobuf v1.5.2
	github.com/k0kubun/pp v3.0.1+incompatible
	github.com/r3labs/diff v1.1.0
)

require (
	github.com/k0kubun/colorstring v0.0.0-20150214042306-9440f1994b88 // indirect
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	golang.org/x/sys v0.0.0-20210927094055-39ccf1dd6fa6 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
)
