#pragma once

extern "C" {
#include "bsp/include/nm_bsp.h"
#include "bsp/include/nm_bsp_fk.h"
#include "driver/include/m2m_periph.h"
#include "driver/include/m2m_ssl.h"
#include "driver/include/m2m_wifi.h"
}