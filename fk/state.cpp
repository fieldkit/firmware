#include "state.h"
#include "state_ref.h"
#include "storage/storage.h"
#include "l10n/l10n.h"

#if defined(__SAMD51__)
#include "hal/metal/metal_ipc.h"
#else
#include "hal/linux/linux_ipc.h"
#endif

extern const struct fkb_header_t fkb_header;

namespace fk {

FK_DECLARE_LOGGER("gs");

DisplayTaskParameters task_display_params;

static GlobalState gs;

void Schedule::recreate() {
    auto has_intervals = false;
    auto cs = lwcron::CronSpec::interval(0);

    for (auto i = 0u; i < MaximumScheduleIntervals; ++i) {
        auto &ival = intervals[i];
        if (ival.interval > 0) {
            for (auto s = ival.start; s <= ival.end; s += ival.interval) {
                lwcron::TimeOfDay tod{ s };
                cs.set(tod);
            }
            has_intervals = true;
        }
    }

    if (!has_intervals && interval > 0) {
        cs = lwcron::CronSpec::interval(std::max(interval, OneMinuteSeconds));
    }

    cron = cs;
}

void Schedule::simple(uint32_t interval) {
    this->repeated = 0;
    this->duration = 0;
    this->jitter = 0;
    this->interval = interval;
    cron = lwcron::CronSpec::interval(std::max(interval, OneMinuteSeconds));
}

Schedule &Schedule::operator=(const fk_app_Schedule &s) {
    for (auto i = 0u; i < MaximumScheduleIntervals; ++i) {
        intervals[i] = Interval{};
    }
    if (s.intervals.arg != nullptr) {
        auto intervals_array = reinterpret_cast<pb_array_t *>(s.intervals.arg);
        auto intervals_source = reinterpret_cast<fk_app_Interval *>(intervals_array->buffer);
        for (auto i = 0u; i < std::min(intervals_array->length, MaximumScheduleIntervals); ++i) {
            if (intervals_source[i].interval > 0 && intervals_source[i].start != intervals_source[i].end) {
                intervals[i].start = intervals_source[i].start;
                intervals[i].end = intervals_source[i].end;
                intervals[i].interval = intervals_source[i].interval;
            }
        }
    }

    interval = s.interval;
    repeated = s.repeated;
    duration = s.duration;
    jitter = s.jitter;

    recreate();

    return *this;
}

GlobalStateRef<const GlobalState *> get_global_state_ro() {
    auto lock = data_lock.acquire_read(UINT32_MAX);
    FK_ASSERT(lock);
    return { std::move(lock), true, &gs };
}

GlobalStateRef<GlobalState *> get_global_state_rw() {
    auto lock = data_lock.acquire_write(UINT32_MAX);
    FK_ASSERT(lock);
    return { std::move(lock), false, &gs };
}

GlobalStateRef<GlobalState const *> try_get_global_state_ro() {
    auto lock = data_lock.acquire_read(0);
    if (!lock) {
        return { std::move(lock), false, nullptr };
    }
    return { std::move(lock), true, &gs };
}

GlobalState::GlobalState() : version(0) {
}

void GlobalState::apply(StorageUpdate &update) {
    storage.meta.size = update.meta.size;
    storage.meta.block = update.meta.records;
    storage.data.size = update.data.size;
    storage.data.block = update.data.records;
    readings.nreadings = update.nreadings;
    readings.time = update.time;
    storage.spi.installed = update.installed;
    storage.spi.used = update.used;

    auto storage_used = ((float)storage.spi.used / (float)storage.spi.installed) * 100.0f;

    loginfo("installed=%" PRIu32 " used=%" PRIu32 " %.2f%% meta-size=%" PRIu32 " meta-records=%" PRIu32 " data-size=%" PRIu32
            " data-records=%" PRIu32 " readings=%" PRIu32,
            storage.spi.installed, storage.spi.used, storage_used, storage.meta.size, storage.meta.block, storage.data.size,
            storage.data.block, readings.nreadings);
}

void GlobalState::apply(UpcomingUpdate &update) {
    scheduler.readings.upcoming = update.readings;
    scheduler.gps.upcoming = update.gps;
    scheduler.network.upcoming = update.network;
    scheduler.backup.upcoming = update.backup;
    scheduler.lora.upcoming = update.lora;
}

void GlobalState::released(uint32_t locked) const {
    auto elapsed = fk_uptime() - locked;
    if (elapsed > 100) {
        loginfo("read (%" PRIu32 "ms)", elapsed);
    }
}

void GlobalState::released(uint32_t locked) {
    logverbose("modified (%" PRIu32 "ms)", fk_uptime() - locked);
    version++;
}

bool GlobalState::flush(uint32_t timeout, Pool &pool) {
    auto lock = storage_mutex.acquire(timeout);
    if (!lock) {
        return false;
    }

    // jlewallen: storage-write
    Storage storage{ MemoryFactory::get_data_memory(), pool, false };
    if (!storage.begin()) {
        return false;
    }

    MetaRecord meta_record{ pool };
    meta_record.include_state(this, &fkb_header, pool);

    if (!storage.meta_ops()->write_record(SignedRecordKind::State, meta_record.record(), pool)) {
        return false;
    }

    if (!storage.flush()) {
        return false;
    }

    return true;
}

GpsState *GpsState::clone(Pool &pool) const {
    auto clone = new (pool) GpsState();
    memcpy(clone, this, sizeof(GpsState));
    return clone;
}

GpsState const *GlobalState::location(Pool &pool) const {
    return gps.clone(pool);
}

NotificationState::NotificationState() {
}

NotificationState::NotificationState(const char *message) : created(fk_uptime()), message(message), delay(0) {
}

NotificationState NotificationState::from_key(uint32_t key) {
    return NotificationState{ en_US[key] };
}

DisplayTaskParameters DisplayTaskParameters::normal() {
    return DisplayTaskParameters{};
}

DisplayTaskParameters DisplayTaskParameters::low_power() {
    DisplayTaskParameters params;
    params.disable_leds = true;
    params.notif = NotificationState::from_key(FK_MENU_LOW_BATTERY);
    return params;
}

} // namespace fk
