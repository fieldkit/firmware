#include <tiny_printf.h>

#include <cstdlib>
#include <cstring>
#include <new>

#include "pool.h"
#include "platform.h"
#include "protobuf.h"
#include "config.h"
#include "memory.h"

void *operator new(size_t size, fk::Pool &pool) {
    return pool.malloc(size);
}

void *operator new[](size_t size, fk::Pool &pool) {
    return pool.malloc(size);
}

void *operator new(size_t size, fk::Pool *pool) {
    return pool->malloc(size);
}

void *operator new[](size_t size, fk::Pool *pool) {
    return pool->malloc(size);
}

namespace fk {

FK_DECLARE_LOGGER("pool");

Pool::Pool(const char *name, size_t size, void *block, size_t taken) {
    name_ = name;
    block_ = block;
    taken_ = taken;
    size_ = size;
    ptr_ = ((uint8_t *)block_) + taken_;
    remaining_ = size_ - taken_;

#if defined(FK_LOGGING_POOL_VERBOSE) || defined(FK_LOGGING_POOL_TRACING)
    if (size_ > 0) {
        loginfo("create: 0x%p %s size=%zu ptr=0x%p taken=%zu", this, name_, remaining_, ptr_, taken_);
    }
#endif
}

Pool::~Pool() {
}

constexpr int32_t MaximumDepth = 4;

void Pool::log_info(int32_t depth) {
    FK_ASSERT(depth < MaximumDepth);
    char prefix[MaximumDepth + 1];
    bzero(prefix, sizeof(prefix));
    for (auto i = 0; i < MaximumDepth; ++i) {
        prefix[i] = i < depth ? '=' : ' ';
    }
    loginfo("info: %s %p '%s' ptr=%p size=%zu remaining=%" PRIu16, prefix, this, name_, ((uint8_t *)block_) + taken_, size_ - taken_,
            (uint16_t)remaining_);
}

void Pool::log_destroy(const char *how) {
#if defined(FK_LOGGING_POOL_VERBOSE) || defined(FK_LOGGING_POOL_TRACING)
    if (size_ > 0) {
        loginfo("destroy: 0x%p %s size=%zu ptr=0x%p (%s)", this, name_, size_ - taken_, ((uint8_t *)block_) + taken_, how);
    }
#endif
}

void Pool::clear() {
#if defined(FK_LOGGING_POOL_VERBOSE) || defined(FK_LOGGING_POOL_TRACING)
    loginfo("clear: 0x%p %s remaining=%zd/%zd", ptr_, name_, remaining_, size_);
#endif
    ptr_ = ((uint8_t *)block_) + taken_;
    remaining_ = size_ - taken_;
#if defined(FK_ENABLE_MEMORY_GARBLE)
    fk_memory_garble(ptr_, remaining_);
#else
    bzero(ptr_, remaining_);
#endif
}

void *Pool::malloc(size_t allocating) {
    FK_ASSERT(allocating > 0);

    auto aligned = aligned_size(allocating);

#if defined(FK_LOGGING_POOL_VERBOSE)
    loginfo("malloc 0x%p %s size=%zu allocating=%zu/%zu remaining=%zu remaining-aligned=%zu", this, name_, size_, allocating, aligned,
            remaining_, remaining_ - aligned);
#endif

    FK_ASSERT(block_ != nullptr);
    FK_ASSERT(size_ > 0);
    FK_ASSERT(remaining_ > 0);
    FK_ASSERT(size_ >= aligned);
    FK_ASSERT(remaining_ >= aligned);

    auto *p = ptr_;
    ptr_ = ((uint8_t *)ptr_) + aligned;
    remaining_ -= aligned;

#if defined(FK_LOGGING_POOL_TRACING)
    loginfo("malloc 0x%p %s size=%zu allocating=%zu/%zu remaining=%zu remaining-aligned=%zu ptr=0x%p", this, name_, size_, allocating,
            aligned, remaining_, remaining_ - aligned, p);
#endif

    return (void *)p;
}

void *Pool::copy(void const *ptr, size_t size) {
    void *newPtr = malloc(size);
    memcpy(newPtr, ptr, size);
    return newPtr;
}

BufferChain Pool::copy(BufferChain const &original) {
    BufferChain clone{ subpool("buff-chain", LinkedBufferSize) };
    clone.head(copy(original.head()));
    return clone;
}

char *Pool::strdup(const char *str) {
    if (str == nullptr) {
        return nullptr;
    }

    auto length = strlen(str);
    auto ptr = (char *)malloc(length + 1);
    strncpy(ptr, str, length + 1);
    return ptr;
}

char *Pool::strndup(const char *str, size_t length) {
    if (str == nullptr) {
        return nullptr;
    }

    auto ptr = (char *)malloc(length + 1);
    strncpy(ptr, str, length + 1);
    ptr[length] = 0;
    return ptr;
}

char *Pool::sprintf(const char *str, ...) {
    va_list args;
    va_start(args, str);
    auto req = tiny_vsnprintf(nullptr, 0, str, args);
    va_end(args);

    auto ptr = (char *)malloc(req + 1);

    va_start(args, str);
    tiny_vsnprintf(ptr, req + 1, str, args);
    va_end(args);

    ptr[req] = 0;
    return ptr;
}

BufferPtr *Pool::copy(BufferPtr const *message) {
    if (message == nullptr) {
        return nullptr;
    }
    BufferPtr *link = message->link() == nullptr ? nullptr : copy(message->link());
    return new (*this) BufferPtr(message->size(), message->position(), (uint8_t *)copy(message->buffer(), message->size()), link);
}

BufferPtr *Pool::buffer(size_t size) {
    return wrap((uint8_t *)malloc(size), size, 0);
}

BufferPtr *Pool::wrap(uint8_t *buffer, size_t size, size_t position) {
    return new (*this) BufferPtr(size, position, buffer);
}

BufferPtr *Pool::encode(pb_msgdesc_t const *fields, void const *src, bool delimited) {
    size_t required = 0;
    if (!pb_get_encoded_size(&required, fields, src)) {
        return nullptr;
    }

    required += delimited ? pb_varint_size(required) : 0;

    auto buffer = (uint8_t *)malloc(required);
    auto stream = pb_ostream_from_buffer(buffer, required);
    if (delimited) {
        if (!pb_encode_delimited(&stream, fields, src)) {
            return nullptr;
        }
    } else {
        if (!pb_encode(&stream, fields, src)) {
            return nullptr;
        }
    }

    FK_ASSERT(stream.bytes_written == required);

    return new (*this) BufferPtr(stream.bytes_written, stream.bytes_written, buffer);
}

void *Pool::decode(pb_msgdesc_t const *fields, uint8_t *src, size_t size, size_t message_size) {
    auto ptr = malloc(message_size);
    auto stream = pb_istream_from_buffer(src, size);
    if (!pb_decode_delimited(&stream, fields, ptr)) {
        return nullptr;
    }

    return ptr;
}

StandardPool::StandardPool(const char *name)
    : Pool(name, StandardPageSize, (void *)fk_standard_page_malloc(StandardPageSize, name), 0u), free_self_{ true } {
}

StandardPool::StandardPool(const char *name, size_t page_size, Pool *page_source)
    : Pool(name, page_size, page_source->malloc(page_size), 0u), free_self_{ false }, page_size_(page_size), page_source_(page_source) {
}

StandardPool::StandardPool(const char *name, void *ptr, size_t size, size_t taken, bool free_self)
    : Pool(name, size, ptr, taken), free_self_{ free_self } {
}

Pool *StandardPool::subpool(const char *name, size_t page_size) {
    auto child = new (this) StandardPool(name, page_size, this);

    if (child_ != nullptr) {
        for (auto iter = child_;; iter = iter->np_) {
            if (iter->np_ == nullptr) {
                iter->np_ = child;
                break;
            }
        }
    } else {
        child_ = child;
    }

    return child;
}

StandardPool::~StandardPool() {
#if defined(FK_LOGGING_POOL_MALLOC_FREE)
    loginfo("free: 0x%p %s size=%zu ptr=0x%p (free=%" PRIu32 ")", this, name(), size(), block(), fk_free_memory());
#endif
    log_destroy("~standard-pool");
    /*
    NOTE No need for this, child/subpools are allocated in parallel to
    our allocations.
    while (child_ != nullptr) {
        auto np = child_->np_;
        delete child_;
        child_ = np;
    }
    */
    if (sibling_ != nullptr) {
#if defined(FK_LOGGING_POOL_VERBOSE) || defined(FK_LOGGING_POOL_TRACING)
        loginfo("standard-pool-delete-sibling: 0x%p sibling=0x%p", this, sibling_);
#endif
        delete sibling_;
        sibling_ = nullptr;
    }
    if (free_self_) {
#if defined(FK_LOGGING_POOL_VERBOSE) || defined(FK_LOGGING_POOL_TRACING)
        loginfo("standard-pool-free-self: 0x%p", this);
#endif
        auto ptr = block();
        if (ptr != nullptr) {
            block(nullptr, 0);
            fk_standard_page_free(ptr);
        }
    }
}

void *StandardPool::malloc(size_t bytes) {
    FK_ASSERT(bytes > 0);
    FK_ASSERT(bytes <= StandardPageSize);

    if (can_malloc(bytes)) {
        return Pool::malloc(bytes);
    }

    if (sibling_ == nullptr) {
        auto overhead = sizeof(StandardPool);
        auto size = page_size_;
        void *ptr = nullptr;
        if (page_size_ > 0) {
            FK_ASSERT(page_source_ != nullptr);
            ptr = page_source_->malloc(page_size_ + overhead);
        } else {
            size = StandardPageSize;
            ptr = fk_standard_page_malloc(size, name());
        }

        // Siblings don't need to free because if they're sourced from
        // another pool, then that pool will free and if they're
        // backed by a page the delete call will free them.
        sibling_ = new (ptr) StandardPool(name(), ptr, size, overhead, false);
    }

    return sibling_->malloc(bytes);
}

void StandardPool::clear() {
    child_ = nullptr;

    if (sibling_ != nullptr) {
#if defined(FK_LOGGING_POOL_VERBOSE) || defined(FK_LOGGING_POOL_TRACING)
        loginfo("standard-pool-clear: sibling=0x%p", sibling_);
#endif
        // If we were allocated as pages from a page source, those
        // can't be freed, so we own those pages still until we're
        // gone forever.
        if (page_size_ > 0) {
            sibling_->clear();
        } else {
            delete sibling_;
            sibling_ = nullptr;
        }
    }

    Pool::clear();
}

void StandardPool::log_info(int32_t depth) {
    Pool::log_info(depth);
    if (sibling_ != nullptr) {
        sibling_->log_info(depth);
    }
    for (auto iter = child_; iter != nullptr; iter = iter->np_) {
        iter->log_info(depth + 1);
    }
}

Pool *create_standard_pool_inside(const char *name) {
    auto size = StandardPageSize;
    auto ptr = fk_standard_page_malloc(size, name);
    auto overhead = sizeof(StandardPool);
    return new (ptr) StandardPool(name, ptr, size, overhead, false);
}

} // namespace fk
