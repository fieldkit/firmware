#pragma once

#include <cinttypes>
#include <cstdlib>
#include <type_traits>
#include <utility>

#include <pb_encode.h>
#include <pb_decode.h>

#include "common.h"
#include "buffers.h"
#include "memory.h"

namespace fk {

constexpr size_t AlignedOn = sizeof(uint32_t);

constexpr size_t aligned_size(const size_t size) {
    return (size % AlignedOn != 0) ? (size + (AlignedOn - (size % AlignedOn))) : size;
}

class Pool {
private:
    const char *name_;
    void *block_;
    void *ptr_;
    size_t remaining_;
    size_t size_;
    size_t taken_;

public:
    explicit Pool(const char *name, size_t size, void *block, size_t taken);
    virtual ~Pool();

public:
    const char *name() const {
        return name_;
    }

    void *block() const {
        return block_;
    }

    void block(void *block, size_t size) {
        block_ = block;
        ptr_ = block;
        size_ = size;
        remaining_ = size;
    }

    void *copy(void const *ptr, size_t size);
    char *strdup(const char *str);
    char *strndup(const char *str, size_t len);
    char *sprintf(const char *str, ...);
    BufferPtr *encode(pb_msgdesc_t const *fields, void const *src, bool delimited = true);
    void *decode(pb_msgdesc_t const *fields, uint8_t *src, size_t size, size_t message_size);
    BufferPtr *copy(BufferPtr const *message);
    BufferChain copy(BufferChain const &original);
    BufferPtr *buffer(size_t size);
    BufferPtr *wrap(uint8_t *buffer, size_t size, size_t position);

public:
    virtual Pool *subpool(const char *name, size_t page_size) {
        FK_ASSERT(0);
        return nullptr;
    }

    virtual size_t allocated() const {
        return used();
    }

    virtual size_t size() const {
        return size_;
    }

    virtual size_t used() const {
        return size_ - remaining_;
    }

    virtual void *malloc(size_t size);

    virtual void clear();

    void *calloc(size_t size) {
        auto ptr = malloc(size);
        bzero(ptr, size);
        return ptr;
    }

    template <typename T> T *malloc() {
        return (T *)malloc(sizeof(T));
    }

    template <typename T> T *malloc_with(T &&value) {
        auto ptr = (T *)malloc(sizeof(T));
        *ptr = std::move(value);
        return ptr;
    }

    template <typename T, size_t N> T *malloc_with(T (&&value)[N]) {
        auto ptr = (T *)malloc(sizeof(T) * N);
        memcpy(ptr, &value, sizeof(T) * N);
        return ptr;
    }

    template <typename T> T *malloc(size_t n) {
        return (T *)malloc(sizeof(T) * n);
    }

public:
    virtual void log_destroy(const char *how);

    virtual void log_info(int32_t depth = 0u);
};

class ScopedClearPool {
private:
    Pool *pool_;

public:
    ScopedClearPool(Pool &pool) : pool_(&pool) {
    }

    ScopedClearPool(Pool *pool) : pool_(pool) {
    }

    virtual ~ScopedClearPool() {
        pool_->clear();
    }
};

class StandardPool : public Pool {
private:
    bool free_self_{ false };
    size_t page_size_{ 0u };
    Pool *page_source_{ nullptr };
    // This pool is where we're allocating from, because we've grown.
    StandardPool *sibling_{ nullptr };
    // This is the first of our children/subpools, used to clear/free.
    StandardPool *child_{ nullptr };
    // This is the next child.
    StandardPool *np_{ nullptr };

public:
    explicit StandardPool(const char *name);
    explicit StandardPool(const char *name, size_t page_size, Pool *page_source);
    explicit StandardPool(const char *name, void *ptr, size_t size, size_t taken, bool free_self);
    virtual ~StandardPool();

public:
    static void operator delete(void *p) {
        fk_standard_page_free(p);
    }

public:
    void log_info(int32_t depth = 0u) override;

    size_t allocated() const override {
        return Pool::allocated() + (sibling_ == nullptr ? 0u : sibling_->allocated());
    }

    size_t size() const override {
        return Pool::size() + (sibling_ == nullptr ? 0u : sibling_->size());
    }

    size_t used() const override {
        return Pool::used() + (sibling_ == nullptr ? 0u : sibling_->used());
    }

    Pool *subpool(const char *name, size_t page_size) override;

    void *malloc(size_t bytes) override;

    using Pool::malloc;

    void clear() override;

    bool can_malloc(size_t bytes) const {
        return (Pool::size() - Pool::used()) >= aligned_size(bytes);
    }
};

Pool *create_standard_pool_inside(const char *name);

} // namespace fk

/**
 * New operator that allocates from a memory pool. Note that this is global
 * because the compiler can't really tell which new or delete you're referring
 * to in all scenarios. Taking a Pool makes things safe, though.
 */
void *operator new(size_t size, fk::Pool &pool);

/**
 * New operator that allocates from a memory pool. Note that this is global
 * because the compiler can't really tell which new or delete you're referring
 * to in all scenarios. Taking a Pool makes things safe, though.
 */
void *operator new[](size_t size, fk::Pool &pool);

/**
 * New operator that allocates from a memory pool. Note that this is global
 * because the compiler can't really tell which new or delete you're referring
 * to in all scenarios. Taking a Pool makes things safe, though.
 */
void *operator new(size_t size, fk::Pool *pool);

/**
 * New operator that allocates from a memory pool. Note that this is global
 * because the compiler can't really tell which new or delete you're referring
 * to in all scenarios. Taking a Pool makes things safe, though.
 */
void *operator new[](size_t size, fk::Pool *pool);
