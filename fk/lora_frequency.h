#pragma once

namespace fk {

enum class lora_frequency_t { Us915, Eu868 };

} // namespace fk
