#pragma once

#include "state/modules.h"

namespace fk {

namespace state {

class DynamicState {
private:
    Pool *pool_{ nullptr };
    AttachedModules *attached_{ nullptr };
    BufferChain events_;

public:
    DynamicState();
    DynamicState(DynamicState &&rhs);
    virtual ~DynamicState();

public:
    AttachedModules *attached() const {
        return attached_;
    }

    BufferChain const *events() const {
        return &events_;
    }

    BufferChain *events() {
        return &events_;
    }

    void events_from(DynamicState &other) {
        events_ = pool_->copy(other.events_);
    }

public:
    DynamicState &operator=(DynamicState &&rhs);
};

} // namespace state

} // namespace fk
